package com.array;

public class ArrayMethod {
	
	public void highestValue(int[] arrayValues) {
		int maxValue=arrayValues[0];
		for (int i = 0; i < arrayValues.length; i++) {
			if(maxValue<arrayValues[i]) {
				maxValue=arrayValues[i];
			}
		}
		
//		for (int value : arrayValues) {
//			if(maxValue<value) {
//				maxValue=value;
//			}
//		}
		System.out.println(maxValue);
	}

	public static void main(String[] args) {
		int value[] = { 3, 56, 32, 1, 2, 3,567, 765,5, 6, 78 };
		ArrayMethod arrayMethod = new ArrayMethod();
		arrayMethod.highestValue(value);

	}
}
