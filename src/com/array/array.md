Arrays:
1.Arrays are homogeneous(single type of data)ie. int ,String.
2.Fixed size. Once we create an array we can't increase or decrease the size.
3. Array is based on index.we can insert or retrive the values.
4. Array index is starts from 0.

5,10,15,20,25,30-->legth=6
index:
0,1,2,3,4,5


Array Declaration:
syntax: int[] referenceVariable=new int[<arraySize?];

datatType[] array;
dataType array[];
Type1: int[] a=new int[6];// declaration and instantiation
		a[0]=5;   ///intilization of the value
		a[1]=10;
		a[2]=15;
		a[3]=20;
		a[4]=25;
		a[5]=30;
		
Type2:int a[]={5,10,15,20,25,30};

int[] a

Two type of arrays:

1.Single Dimensional Array
2.Multidimensional Array

 int[][]  a=new int[2][5];// declaration and instantiation
a[0][0]=2;
a[0][1]=2;
a[0][2]=2;
a[0][3]=2;
a[0][4]=9;
a[0][5]=78;
a[1][0]=2;
a[1][1]=2;