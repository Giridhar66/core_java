package com.array;

public class ReturnArray {

	public int[] returnArrayValues() {
		int a[]= {1,3,5,745,32};
	return a;
	}
	
	public static void main(String[] args) {
		ReturnArray r=new ReturnArray();
		int[] returnArrayValues = r.returnArrayValues();
		
		for (int i : returnArrayValues) {
			System.out.println(i);
		}
		
	}
}
