package com.variable;

public class Variable {
	public int a = 100;//Instance variable or global variable

	public static float val=14.4f;  //static variable
	
	public void m1() {
		int a=50;//local variables
		System.out.println("access in method level::"+this.a);
	}
	
	public String s="giri";//Instance variable
	
	public static void main(String[] args) {
		int a=10;//local variables
		boolean flag=false;//local variable
		
		//syntax: ClassName referenceVariable=new ClassName();
		Variable vari=new Variable();
		//calling instance variable---call through the object reference
		System.out.println(vari.a);
		//call the static variable 2ways
//		1.through the object reference 2. By using class name
		System.out.println(Variable.val);
		//local variable calling.
		System.out.println(a);
		System.out.println(flag);
		//calling method.
		vari.m1();
		
	}
	
}
