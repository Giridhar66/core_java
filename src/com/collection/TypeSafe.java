package com.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TypeSafe {
	public static void main(String[] args) {
		List<Integer> p = new ArrayList<Integer>();
		p.add(12);
		p.add(23);
		p.add(54);
		p.add(67);
		p.add(90);

		// Iterate the List Object:
//	1.forLoop
//	2.ForEach
//	3.Iterator
//	4.ListIterator
		Iterator<Integer> it = p.iterator();
		while (it.hasNext()) {
			Integer next = it.next();
			// down casting::
//			Integer in = (Integer) next;
			System.out.println(next);
		}

	}
}
