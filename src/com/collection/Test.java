package com.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Test {
	
	public static void main(String[] args) {
		
		List p=new ArrayList();
		p.add(12);
		p.add("giri");
		p.add(true);
		p.add(23);
		p.add(54);
		p.add("Reddy");
//		System.out.println(p);//[12, giri, true, 23, 54, Reddy]
		
		//Iterate the List Object:
//		1.forLoop
//		2.ForEach
//		3.Iterator
//		4.ListIterator
		Iterator it = p.iterator();
		while(it.hasNext()) {
			Object next = it.next();
			System.out.println(next);
		}
		
	}

}
