#Constructor:

Rules:
1. Class name and constructor name must be same.
2. There should not be any return type.
3. should not give any abstract,static, final.
Test t=new Test();

Syntax:

(<Access Modifier> <option>)(default) <ClassName>(){

}

Test(){

}
#Calling constructor:
While object creation time constructor will be call.

#Types Of constructor: 

1.Default constructor or (0-args constructor).
2. Parameterized constructor.

#Overloading constructor

Constructor name is same but different paramerters.

#this

When i want call the current class variable or method.
#super keyword

to access the parent class variable and methods.

#static Block

static{
//logic
}
#instance block
{
System.out.println("instance block");
}
