package com.constructor;

public class ThisEx {

	int a;//instance variable
	int b;
	
	ThisEx(int val, int val2){
		this.a=val;
		this.b=val2;
	}
	public void m1() {
		//logic
		System.out.println("m1::");
		this.m2();
	}
	public void m2() {
		System.out.println(this.a);
		System.out.println("m2 method::");
	}
	
	public static void main(String[] args) {
		ThisEx ex=new ThisEx(10,20);
		ex.m1();
		System.out.println(ex.a);
		System.out.println(ex.b);
	}
}
