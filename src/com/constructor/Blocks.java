package com.constructor;

class Parent1{
	Parent1(){
		System.out.println("Parent constructor");
	}
}

public class Blocks  extends Parent1 {

	static {
		System.out.println("static block");
	}
	{
		System.out.println("instance block");
	}
	public static void main(String[] args) {
		Blocks b=new Blocks();
	}
	
}
