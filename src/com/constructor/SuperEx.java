package com.constructor;

class Parent {
	String name="Giri";
	
	Parent(){
		System.out.println("parent constructor");
	}
	Parent(int i){
		System.out.println("parent constructor");
	}
	
	public void parentM1() {
		System.out.println("parentM1");
	}
	
}

public class SuperEx extends Parent {
	String name="Reddy";
	
	SuperEx(){
		super(10);
		System.out.println("SuperEx constructor");
	}
	
	public void displayName() {
//		System.out.println(this.name);
//		System.out.println(super.name);
		super.parentM1();
	}
	
	public static void main(String[] args) {
		SuperEx su=new SuperEx();
		su.displayName();
	}
}
