package com.constructor;

public class Test {
	Test() {
		System.out.println("default constructor");
	}

	Test(int val1, int val2){
		System.out.println("parameterized const");
	}
	
	Test(boolean val, int val2){
		System.out.println("overloaded boolean parameterized const");
	}
	Test(boolean val, int val2,String s){
		System.out.println("overloaded boolean,String parameterized const");
	}
	public static void main(String[] args) {

		Test t = new Test();
		Test t1=new Test(1,2);
		Test t2=new Test(false,2);
		Test t3=new Test(false,2,"Giridhar");
//		t.m1();
	}

}
