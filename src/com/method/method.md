

Instance Method:
syntax:
--------

<accessModifier> <Return Type> <method Name>( method parameters){
//business logc.
}
Method parameter as args: <DataType> referenceVariable
dataTypes:
int
byte
short
long
flot
double
boolean.

public void m1(){  0-args method

}
calling Instance method:
--
Through the object reference we have to Instance method:
Test t=new Test(); //Object creation
t.m1();

static Method:
<accessModifier> <static> <Return Type> <method Name>( method parameters){
//business logc.
}
public static void m1(){

}

calling static method:
--
1.Through the object reference we have to Instance method:
Test t=new Test(); //Object creation
t.m1();
2.By using class name ---Object creation is not required. 
Test.m2();

class
-----
variables--local variable, instance variable, static variable, final variable, final+static= constant
methods--main(), instance method, static method.
block- static and instance block
Constructors.- default constructor, parameterized constructor.
abstract methods.



