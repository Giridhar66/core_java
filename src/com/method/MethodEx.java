package com.method;

/**
 * 
 * @author sgiri
 *
 */
public class MethodEx {
//comment--
//	methods maybe 1 max.
	
	public void m1() {
		System.out.println("m1-method");
	}
	public static void m2() {
		System.out.println("m2-method");
	}
	
	public void calc(int firstVal,int  secondVal) {
		System.out.println(firstVal+secondVal);
	}
	public int calc(int firstVal, int secondVal, int thirdVal) {
		//business logic
		int total=firstVal+secondVal+thirdVal;
		//hard coated value or int type reference
		return total;
	}
	
	public static void main(String[] args) {
		//syntax: ClassName referenceVariable=new ClassName();
		MethodEx ex=new MethodEx();
		ex.m1();
		//way of calling static method..
//		ex.m2();
		MethodEx.m2();
//		ex.calc(5, 6);
		
	int sum=ex.calc(1, 2, 3);
	System.out.println(sum);

	}

}
