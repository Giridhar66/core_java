package com.method;
class Parent{
	Parent(){
		System.out.println("parent const");
	}
	
	
}

public class Test extends Parent {
	Test(){
//		super();
		System.out.println("constructor");
	}
	Test(int i){
		super();
		System.out.println("single parameter");
	}
	static {
		System.out.println("static bloc is executed");
	}
	{
		System.out.println("instance");
	}
public static void main(String[] args) {
	
	Test t=new Test();
	Test t1=new Test(10);
	
}
}
