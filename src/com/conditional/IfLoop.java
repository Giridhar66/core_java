package com.conditional;

import java.util.Scanner;

public class IfLoop {
	
public void m1() {
	System.out.println("Calling m1() for business service..");
}
	
	public static void main(String[] args){
//		int age=18;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter age:");
		int age = s.nextInt();
		if(age>18) {
			//Business logic
			System.out.println("Eligible for vote::");
//			Test t=new Test();
//			t.m1();
		}else {
			System.out.println("Not Eligible for vote::");
		}
		
	}
	
}
