package com.conditional;

public class ForLoop {

	public static void main(String[] args) {
		//print values 1-10;
		for(int i=0;i<=10;i++) {
			
			if(i==5) {
				break;
//				continue;
				}
		
			System.out.println(i);
		}
		System.out.println("out side of the for loop");
	}
}
