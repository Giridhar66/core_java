package com.abstrac;

public class BikeImpl extends Bike {

	@Override
	public void engineImpl() {
		System.out.println("Implemented engine impl in bike Impl class.");
	}

	public static void main(String[] args) {

		Bike b=new BikeImpl();//upcasting.
		b.engineImpl();
		b.engine();
		
	}

}
