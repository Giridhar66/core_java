package com.abstrac;

abstract class Test1{
	
}
abstract class Test2{
	
}
//java does not support multiple inheritance.
//public abstract class Test extends Test1,Test2  {
public abstract class Test  {

	public abstract void m1();
	public abstract void m2();
	public abstract void m3();
	public abstract void m4();
	public abstract void m5();
	//Normal method
	public  void m6() {
		System.out.println("m6");
	}
	public  void m7() {
		System.out.println("m7");
	}
	public  void m8() {
		System.out.println("m8");
	}
}
