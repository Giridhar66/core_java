package com.abstrac;

public abstract class Bike {
	
	// can we create constructor for abstract class
	Bike(){
	System.out.println("constructor");	
	}
	
	//normal method or non abstract method.
	public void engine() {
		System.out.println("Implentation of engine");
	}

	//Abstract class
	public abstract void engineImpl();
	
	
}
