package com.polymorphism;

public class Employee {

	private int employeeId;
	private String employeeName;
	private int employeeSalary;
	
	public void setEmployeeId(int employeeId) {
		this.employeeId=employeeId;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName=employeeName;
	}
	
	public int getEmployeeId() {
		return employeeId;
	}
	
	public String getEmployeeName() {
		return employeeName;
	}
	
	@Override
	public String toString() {
		
		return "[EmployeeId="+employeeId+","+"EmployeeName="+employeeName+","+"EmployeeSalary="+employeeSalary+"]";
	}
	
	public static void main(String[] args) {

		Employee e = new Employee();
		e.setEmployeeId(120);
		int getEmp = e.getEmployeeId();
		System.out.println(getEmp);
		e.setEmployeeName("Ramesh");
		System.out.println(e.getEmployeeName());
		System.out.println(e);
	}

	
}
