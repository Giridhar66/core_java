package com.polymorphism;

class Parent2 {

	String name="giri";
	void showMessaage() {
		System.out.println("Parent class showMessage");
	}
}

public class DownCastChild extends Parent2 {
	int age=24;
	void showMessaage() {
		System.out.println("upCastChild class showMessage");
	}

	public static void main(String[] args) {

		Parent2 p = new DownCastChild();
		DownCastChild down=(DownCastChild)p;
		down.showMessaage();
		System.out.println(down.name);

	}
}
