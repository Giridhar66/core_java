package com.polymorphism;

class A {
	void property() {
		System.out.println("50CR");
	}
}
class B extends A{
	
	void landProp() {
		System.out.println("10ACRE");
	}
}

public class MultiLevel extends B{
	public static void main(String[] args) {
		
		MultiLevel mu=new MultiLevel();
		mu.property();
		mu.landProp();
	}
	

}
