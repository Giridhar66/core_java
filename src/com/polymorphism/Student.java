package com.polymorphism;

public class Student {

	public void student() {
		System.out.println("student");
	}
	
	public void student(String school) {
		System.out.println("school");
	}
	public void student(int val) {
		System.out.println("school");
	}
	public void student(String school,String work) {
		System.out.println("school+work");
	}
	
	public void m1(int i, String j) {
		System.out.println("2 args method");
	}
	public void m1(String i, int j) {
		System.out.println("2 args method String+int");
	}
	
	public static void main(String[] args) {
		
		Student st=new Student();
		st.student("school name");
		
	}
}
