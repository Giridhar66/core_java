package com.polymorphism;

class Vehicle{
	
	public void run() {
		System.out.println("vehicle Running");
	}
	
}

public class Bus extends Vehicle{

	@Override
	public void run() {
		System.out.println("Bus Running");
	}
	
	public static void main(String[] args) {
		//ParentClass p=new ChildClass();
//		Bus b=new Bus();
		Vehicle v=new Bus();
		v.run();
	}
}
