#Overload methods
// compile time polymorphism.
1. Changing the number of method arguments.
2. By changing the data type.
Ex: 

public void m2(){  -- zero arg method.
}

public void m2(int a,String b){ 
}
public void m2(int a,String b,int j){ 
}

public void m2(String a,int b){ 
}

#overriding
//Run time polymorphism 
1. method name must be same and parameters must be same.

#Encaptulation:

rules:
1. private instance variables
2. should be write setter method and getting method
3. toString method.

