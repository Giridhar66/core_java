package com.polymorphism;
class RBI{
	
	public int rateOfInterest() {
		return 1;
	}
}
class HDFCBank extends  RBI{
	@Override
	public int rateOfInterest() {
		return 6;
	}
}
class ICICIBank extends  RBI{
	@Override
	public int rateOfInterest() {
		return 9;
	}
}


public class Test {
	public static void main(String[] args) {
		HDFCBank hdfc=new HDFCBank();
		int rate=hdfc.rateOfInterest();
		System.out.println(rate);
	}

}
