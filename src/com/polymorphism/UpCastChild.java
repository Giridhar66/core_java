package com.polymorphism;

class Parent {

	void showMessaage() {
		System.out.println("Parent class showMessage");
	}
}

public class UpCastChild extends Parent {
	void showMessaage() {
		System.out.println("upCastChild class showMessage");
	}

	public static void main(String[] args) {

		Parent p = new UpCastChild();
		Parent p1 = (Parent)new UpCastChild();
		p.showMessaage();
		p1.showMessaage();

	}
}
