Process of converting one data type to another data type is known as type casting .

PARENT------------>Child(Upcasting)--
Child------------->Parent(Downcasting)

#UpCasting:
A child object is Typecasted to a parent object

Parent p=new Child();//implicit casting
Parent p=(Parent)new Child;//Explict casting.

#Downcasting:
Parent class reference object we have pass to child class

Parent p=new Child();
Child c=(Child)P;//Explicitly casting.

-->Implictly not posible.

-->We can able to access all the methods and variable.