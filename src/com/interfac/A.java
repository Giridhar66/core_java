package com.interfac;
 interface B{
	 int  b=20;
	public void b1();
	public void b2();
	public void b3();
	
}
 interface C{
	 public void c1();
	 public void c2();
	 public void c3();
	 
 }
//Multiple inheritance
public interface A  extends B,C {
	int a = 10;
	String STUDENT_NAME="Giridhar";
	//abstract keyword is optional at method level
	public abstract void m1();
	public void m2();
	
	
}
