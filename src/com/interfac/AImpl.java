package com.interfac;

public class AImpl implements A  {

	@Override
	public void b1() {
		System.out.println("b1 method");
		
	}

	@Override
	public void b2() {
		System.out.println("b2 method");		
	}

	@Override
	public void b3() {
		System.out.println("b3 method");
		
	}

	@Override
	public void m1() {
		System.out.println("m1 method");		
	}

	@Override
	public void m2() {
		System.out.println("m2 method");
		
	}
	
	public static void main(String[] args) {
		A a=new AImpl();//Upcasting.
		a.b1();
		a.b2();
		
	}

	@Override
	public void c1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void c2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void c3() {
		// TODO Auto-generated method stub
		
	}

}
