package com.exception;

public class Number {

	public void stringLenght() {
		String s = "giridhar";
		System.out.println(s.charAt(8));

	}

	
	public void stringNull() {
		String s = null;
		System.out.println(s.length());
	}
	
	public void numberFormat() {
		String s = "asdf";
		try {
			String s1 = "123a";
			int i = Integer.parseInt(s1);
			System.out.println(i);
		} catch (StringIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
		} 
	}
	
	public void arrayIndex() {
		int a[]=new int[2];
		a[3]=2;
//		a[1]=4;
	
	}
	
	
	public void checkArithmatic() {
		System.out.println("befor try block");
		try {
			System.out.println(10/2);//Statement1
			System.out.println(20/4);//Statement2
			System.out.println(18/6);//Statement3
//			System.out.println(10/0);//Statement4
//			String s = null;
//			System.out.println(s.length());
			String s = "giri";
			System.out.println(s.charAt(7));
			System.out.println(20/4);//Statement5
			
		}catch (ArithmeticException e) {
			  e.getMessage();
		} catch (NullPointerException e) {
			System.out.println("NullPointerException block:: " + e);
		}catch (Exception e) {
			e.printStackTrace();
//			System.out.println(e.printStackTrace());
		}
		finally {
			System.out.println("Finally block");
		}
		System.out.println("after try block");
	}

	public static void main(String[] args) {
		Number nu=new Number();
//		nu.checkArithmatic();
		nu.numberFormat();
//		nu.arrayIndex();
//		nu.stringLenght();
//		nu.stringNull();
	}

}
