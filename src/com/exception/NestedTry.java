package com.exception;

public class NestedTry {

	public static void main(String[] args) {
		try {
			System.out.println(10 / 2);
//			String s = "giri";
//			System.out.println(s.charAt(7));
			try {
				System.out.println("inner try block");
			} catch (Exception e) {
				System.out.println("exception");
			}
		} catch (Exception e) {
			try {
				System.out.println("In Catch block source");
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		finally {
			System.out.println("finally block");
			try {
				System.out.println("finaly-try");
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
	}
}
