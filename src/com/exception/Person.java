package com.exception;

import java.util.Scanner;

class NotEligibleForMarriage extends Exception {

	NotEligibleForMarriage(String s){
		super(s);
	}
}

public class Person {

	public void checkEligibleForMarriage(int age) throws NotEligibleForMarriage {
		if (21 < age) {
			System.out.println("Eligible for marriage");
		} else {
			throw new NotEligibleForMarriage("Not Eligible for marriage");
//			throw new Exception("Not Eligible for marriage");
		}
	}

	public static void main(String[] args)  throws NotEligibleForMarriage{
		Person p = new Person();
		Scanner s=new Scanner(System.in);
		System.out.println("Enter age");
		int age = s.nextInt();
		p.checkEligibleForMarriage(age);
	}
}
