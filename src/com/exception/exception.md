Exception:

Abnormal Termination.

Exception :


try
catch
finally
throw
throws

try:
exceptional code will write try block
catch:
to catch the exception will use catch block
finally:
to release the resource or clean up the code .


Rules:
1.when we declare try block must be declare catch block
2. try block have multiple catch blocks.
3. in between try and catch it should not allow any statements

UserDefineException||Custom Exception
throw
throws

throw:
Method Inside we have to declare and throw an exception explicitly in the code,
throw key word to use and customized exception.
throw keyword we can declare only once in method inside.

throws:
throws keywords we will write in method signature it self.
Exception followed by class name.
We can declare multiple excepiton using throws keyword




